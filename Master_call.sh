#!/bin/sh

export FSLOUTPUTTYPE=NIFTI_GZ

if [ $# -lt 1 ] ; then
echo "-------------- Master_call.sh -------------------"
echo "This script contains the pipeline used for the analyses of the paper:"
echo "White matter hyperintensities classified according to intensity and spatial location reveal specific associations with cognitive performance" 
echo "by Melazzini et al."
echo "Setup files and paths according to your data structure then can be used calling:"
echo "Master_call.sh <subject number>"
echo "----------------------------------------------------"
exit -1
fi

#### SETUP files and paths (modify paths and filenames accordingly)

# Set the path of the directory containing the scripts
scriptsdir=
# subjects details
subjnum=$1
sub=sub-${subjnum}
mydir=<absolute path>/${sub} 

# setting up output directory
subdir=${sub}_WMHsubclasses
mkdir ${mydir}/${subdir}
outdir=${mydir}/${subdir}

# FLAIR image
flairimage=${mydir}/anat/${sub}_FLAIR.nii.gz
# Partial Volume Estimate map of white matter (output of FAST or fsl_anat)
WMpve=${mydir}/anat/${sub}_T1w.anat/T1_fast_pve_2.nii.gz
# Transformation from T1w to FLAIR image
T1toFLAIRmat=${mydir}/anat/${sub}_T1w_biascorr_brain_2_FLAIR.mat
# WMH mask (BIANCA output thesholded and binarised)
biancafile=${mydir}/anat/${sub}_bianca_output_bin09.nii.gz


# required for pvent/deep
GRANDEX=${scriptsdir}/GRANDEX2.nii.gz
# non linear warp from MNI to T1w (output of fsl_anat or derived separately)
MNI2T1=${mydir}/anat/${sub}_T1w.anat/MNI_to_T1_nonlin_field.nii.gz

#required for DTI analysis
B0toFLAIRmat=${mydir}/dwi/${sub}_B0_2_FLAIR.mat
FA=${mydir}/dwi/dti/${sub}_dti_FA.nii.gz
MD=${mydir}/dwi/dti/${sub}_dti_MD.nii.gz
L1=${mydir}/dwi/dti/${sub}_dti_L1.nii.gz
L2=${mydir}/dwi/dti/${sub}_dti_L2.nii.gz
L3=${mydir}/dwi/dti/${sub}_dti_L3.nii.gz

#### END SETUP files and paths 

#1- Voxel-wise subclassification of WMH according to T1 intensity
id1=`fsl_sub -q veryshort.q ${scriptsdir}/step1_T1subclass_voxelwise $biancafile $flairimage $WMpve $T1toFLAIRmat $outdir`
#output images:
#${outdir}/WMH_T1hypo_PVE_2.nii.gz = WMH T1-hypointense
#${outdir}/WMH_no_T1hypo_PVE_2.nii.gz = WMH T1-isointense
#output volumes saved in #${outdir}/volumes.txt

#2- Subclassification of WMH in 4 classes
id2=`fsl_sub -j $id1 -q long.q ${scriptsdir}/step2_4subclasses $biancafile $flairimage ${outdir}/WMH_T1hypo_PVE_2.nii.gz $MNI2T1 $T1toFLAIRmat $GRANDEX $outdir`
#output images:
#${outdir}/perivent_map_adj.nii.gz = WMH periventricular
#${outdir}/deepwm_map_adj.nii.gz = WMH deep

#${outdir}/WMH_perivent_T1hypo_PVE_2.nii.gz = WMH periventricular T1 hypointense
#${outdir}/WMH_perivent_no_T1hypo_PVE_2.nii.gz = WMH periventricular non-T1 hypointense
#${outdir}/WMH_deep_T1hypo_PVE_2.nii.gz = WMH deep T1 hypointense
#${outdir}/WMH_deep_no_T1hypo_PVE_2.nii.gz = WMH deep non-T1 hypointense
#output volumes added to #${outdir}/volumes.txt

#3- DTI metrics in T1-hypointense/T1-isointense WMH
id3=`fsl_sub -j $id1 -q veryshort.q ${scriptsdir}/step3_DTI_T1classes $flairimage $FA $MD $L1 $L2 $L3 $B0toFLAIRmat $outdir`
#output images are DTI maps to FLAIR
#output file ${outdir}/DTI_metrics.txt contains metrics for T1hypointense and non-T1hypointense WMH

#4- Cluster-wise subclassification according to T1 intensity
# (i.e. WMH clusters including hypointense voxels vs WMH clusters not including T1-hypointense voxels)
fsl_sub -j $id1 -q short.q ${scriptsdir}/step4_T1subclass_clusters $biancafile ${outdir}/WMH_T1hypo_PVE_2.nii.gz $outdir