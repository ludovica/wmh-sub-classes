# WMH sub-classes

This repository contains the code used for white matter hyperintensities sub-classification presented in our [Neuroimage:Clinical paper](https://www.sciencedirect.com/science/article/pii/S2213158221000607?via%3Dihub):

> "White matter hyperintensities classified according to intensity and spatial location reveal specific associations with cognitive performance"
>
> *Luca Melazzini, Clare E Mackay, Valentina Bordin, Sana Suri, Enikő Zsoldos, Nicola Filippini, Abda Mahmood,
Vaanathi Sundaresan, Marina Codari, Eugene Duff, Archana Singh-Manoux, Mika Kivimäki, Klaus P Ebmeier, Mark Jenkinson,
Francesco Sardanelli, Ludovica Griffanti.*


## How to cite:
Our paper is available open access [here](https://www.sciencedirect.com/science/article/pii/S2213158221000607?via%3Dihub) (doi: https://doi.org/10.1016/j.nicl.2021.102616)

## Contents:
- `Master_call.sh`: main script. Contains a setup section to modify according to your paths and filenames (see pre-processing below) and calls the other scripts.
- `step1_T1subclass_voxelwise`: classifies WMH voxels into those that are T1-hypointense and those that are T1-isointense (or non T1-hypointense).
- `step2_4subclasses`: further classifies WMH into periventricular and deep, according to the continuity to ventricle rule. It uses an extended vetricles mask in MNI space `GRANDEX2.nii.gz`.
- `step3_DTI_T1classes`: calculates average DTI metrics in T1-hypointense and T1-isointense (or non T1-hypointense) WMH voxels
- `step4_T1subclass_clusters` : alternative classification according to T1 intensity. It classifies WMH into clusters that contain a T1-hypointense component (i.e. clusters that are, at least partially, T1-hypointense) and those that are T1-isointense (i.e. only contain non T1-hypointense voxels).


## Pre-processing:
These scripts assume your data has already gone through the some preprocessing steps:
- WMH segmentation in FLAIR space to obtain WMH binary mask (e.g. with FSL BIANCA + thresholding and binarisation)
- T1 tissue-type segmentation (e.g. with FSL FAST) to obtain the white matter partial volume estimate
- calculation of the linear transformation (.mat) from T1 to FLAIR
- calculation of the nonlinear transformation (warp) from MNI to FLAIR (for step 2)
- DTI preprocessing and calculation of maps of FA, MD, L1, L2, L3 (for step 3)

## How to run:
- Set up the paths in the first section of `Master_call.sh`
- Run `Master_call.sh` giving as input the subject number
- To run only some steps, comment out / delete calls to the commands as appropriate


